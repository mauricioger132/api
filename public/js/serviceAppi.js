'use strict'

var urlApi;
var contentHTML="";
var count=0;

function clearn(){
	$("#id").val("");
	$("#code").val("");
	$("#name").val("");
	$("#address").val("");
	$("#email").val("");
	$("#alertBranch").empty();
}
function typeData(datatype,cont){
	if (datatype == undefined && cont == undefined){
		urlApi=`https://gateway.marvel.com:443/v1/public/comics?format=comic&formatType=comic&orderBy=title&ts=1&apikey=14cfef4a2ed0248b39a2f9897cbe6dd8&hash=ddc175e39e993b7af94eb8f6856aa1cb`;  
		getListComics(urlApi);

	}else{
		var param=$( `.params${cont}`).val();
		urlApi=`https://gateway.marvel.com:443/v1/public/comics/${param}?&ts=1&apikey=14cfef4a2ed0248b39a2f9897cbe6dd8&hash=ddc175e39e993b7af94eb8f6856aa1cb`;
		viewDetails(urlApi);
	}
}
function getListComics(urlApi){
	fetch(urlApi).then(data=>data.json())
	.then((json)=>{
		
		json.data.results.map(data=>{
			let urlcomics=data.urls[0].url;
			contentHTML=`<div class=" card col-md-3" id="fila" style="margin-bottom:5px;margin-left: -3px;border:#d81111  2px solid; !important">
                			<img src="${data.thumbnail.path}.${data.thumbnail.extension}" class="card-img-top" alt="${data.title}" style="margin-top:12px !important">
  							<div class="card-body">
    							<h5 class="card-title">${data.title}</h5>
   								<input type="hidden" name="params" class="params${count}" value="${data.id}" />
    							<button type="button"  class="btn btn-primary information" onclick="typeData(1,${count})" >Ver detalles</button>
 							 </div
						</div>`;

          $("#comicsRows").append(contentHTML); 
          count++;
		})
		
	})
}
function viewDetails(urlApi){	
	$("#comicstable").empty(); 
	fetch(urlApi).then(data=>data.json())
		.then((json)=>{
			var dates =json.data.results[0].dates[0].date.split('T');
			var date=dates[0];		
			getDetailSucursal(json.data.results[0].id).then((data)=>{
				var namesucursales=[];
				data.map((sucursal)=>{
					namesucursales.push(sucursal.name);
				})
				var sucursalesDis=namesucursales.toString();
				$('#modalcomics').modal({backdrop: 'static', keyboard: false});
				$("#modalcomics").modal("show");
				var contentHTMLs=`<tr> 
									<td rowspan="4" style="width:20%; !important " ><img src="${json.data.results[0].thumbnail.path}.${json.data.results[0].thumbnail.extension}" class="card-img-top" alt="${json.data.results[0].title}" style="margin-top:12px; width: 100%; !important"></td>
									<td><p>${json.data.results[0].title}  No.Pag. ${json.data.results[0].pageCount}</p></td>
									<td><p>Fecha de lanzamiento : ${date}</p></td>
								</tr>
								<tr> 
									<td colspan="2"><p>${json.data.results[0].description}</p></td>
								</tr>
								<tr>
									<td><p>Sucursales disponibles: <br><b>${sucursalesDis}</b></br></p></td>
									<td><p>Personajes especiales : <b>${json.data.results[0].characters.collectionURI}</b></p></td>
								</tr>`;	
	    		$("#comicstable").append(contentHTMLs); 
			})
		})
}
function getDetailSucursal(idcomic){
    return new Promise((ejecuta)=>{
        setTimeout(function(){
            $.get(urlgetbranchDetail,{'idcomic':idcomic},(data)=>{
                return ejecuta(data);
            })
        },500);       
    });
}

$("#addBranch").on('click',(e)=>{
	$("#alert").empty();
	e.preventDefault();
	var formData = new FormData($("#registerBranch")[0]);
    $.ajax({
    	headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    	url:urladdBranch,
    	type: "post",
    	dataType: "html",
    	data: formData,
    	cache: false,
    	contentType: false,
    	processData: false,
    	success:function(data)
    	{
			contentHTML=`
			    		<div class="alert alert-success alert-dismissible fade show" role="alert" id="alert">
							<strong>Registro guardado satisfactoriamente!</strong> 
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>`;
    		$("#alert").append(contentHTML);
    		clearn();
    	},
    	error:function(data)
    	{
	    	contentHTML=`
	    	    		<div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert">
							 <strong>LLena todos los campos!</strong> 
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>`;
	    	$("#alert").append(contentHTML);
    	}
  	})
	
})

function modalbranch(){
	$("#rowsBranch tbody").empty();
	$('#modalbranchs').modal({backdrop: 'static', keyboard: false});
	$("#modalbranchs").modal("show");
	$.get(urlgetBranch,(data)=>{
		 data.map((data)=>{
		 	contentHTML=`<tr>
				      		<th scope="row">${data.code}</th>
				      		<td>${data.name}</td>
				      		<td>${data.address}</td>
				      		<td>${data.email}</td>
				      		<td>
				      			<div>
				      			     <button type="button" class="btn btn-warning btn-sm" onclick="getRegisters(${data.id})">Modificar</button>&nbsp;
				      			     <button type="button" class="btn btn-danger btn-sm" onclick="deleteBranch(${data.id})">Eliminar</button>
				      			</div>
				      		</td>
				    	</tr>`
			$("#rowsBranch tbody").append(contentHTML);
		 })
	})
}
function getRegisters(id){
	$("#alertBranch").empty();
	$.get(urlgetRegisters,{'id':id},(data)=>{
		$("#alertBranch").show();
    	 contentHTML=`
    	    		<div class="alert alert-success alert-dismissible fade show" role="alert" id="alert">
						 <strong>Datos cargados correctamente da click en cerrar!</strong> 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
    	`;
    	$("#alertBranch").append(contentHTML);
		$("#id").val(data[0].id);
		$("#code").val(data[0].code);
		$("#name").val(data[0].name);
		$("#address").val(data[0].address);
		$("#email").val(data[0].email);
	})
}
function  clearAlert(){
	$("#alertBranch").hide();
}
function deleteBranch(id){
	$("#alertBranch").empty();
	$.get(urlDeleteBranch,{'id':id},(data)=>{
		$("#alertBranch").show();
		if (data!="" || data!=undefined){
    		contentHTML=`
    	    			<div class="alert alert-success alert-dismissible fade show" role="alert" id="alert">
							 <strong>Dato eliminado correctamente!</strong> 
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>`;
    	$("#alertBranch").append(contentHTML);
		$("#rowsBranch tbody").empty();
		$.get(urlgetBranch,(data)=>{
			 data.map((data)=>{
			 	contentHTML=`<tr>
					      		<th scope="row">${data.code}</th>
					      		<td>${data.name}</td>
					      		<td>${data.address}</td>
					      		<td>${data.email}</td>
					      		<td>
					      			<div>
					      			     <button type="button" class="btn btn-warning btn-sm" onclick="getRegisters(${data.id})">Modificar</button>&nbsp;
					      			     <button type="button" class="btn btn-danger btn-sm" onclick="deleteBranch(${data.id})">Eliminar</button>
					      			</div>
					      		</td>
					    	</tr>`
				$("#rowsBranch tbody").append(contentHTML);
			 })
		})

		}else{
			    	contentHTML=`
    	    		<div class="alert alert-warning alert-dismissible fade show" role="alert" id="alert">
						 <strong>El dato no se pudo elimina!</strong> 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
    	`;
    	$("#alertBranch").append(contentHTML);
		}	
	})
}
function getRegistersEn(){
	$("#alertBranch").empty();
	$("#rowsBranch tbody").empty();
	var id =$("#getRegisterUnic").val();
	$.get(urlgetsbranches,{'id':id},(data)=>{
	
		if (data.val ==false && data.getRegisterBranch!="" ){

	
			contentHTML=`<tr>
					      <th scope="row">${data.getRegisterBranch[0].code}</th>
					      		<td>${data.getRegisterBranch[0].name}</td>
					      		<td>${data.getRegisterBranch[0].address}</td>
					      		<td>${data.getRegisterBranch[0].email}</td>
					      		<td>
					      			<div>
					      			     <button type="button" class="btn btn-warning btn-sm" onclick="getRegisters(${data.getRegisterBranch[0].id})">Modificar</button>&nbsp;
					      			     <button type="button" class="btn btn-danger btn-sm" onclick="deleteBranch(${data.getRegisterBranch[0].id})">Eliminar</button>
					      			</div>
					      		</td>
					    	</tr>`;
					    				$("#rowsBranch tbody").append(contentHTML);
		}else if (data.val ==false && data.getRegisterBranch=="") {

		
			var ALERTHTML=`
    	    		<div class="alert alert-warning alert-dismissible fade show" role="alert" id="alert">
						 <strong>No existen datos!</strong> 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>`;
				$("#alertBranch").append(ALERTHTML);

		}else if(data.val ==true && data.getRegisterBranch!=""){
			data.getRegisterBranch.map((data)=>{
	 	contentHTML=`<tr>
				      		<th scope="row">${data.code}</th>
				      		<td>${data.name}</td>
				      		<td>${data.address}</td>
				      		<td>${data.email}</td>
				      		<td>
				      			<div>
				      			     <button type="button" class="btn btn-warning btn-sm" onclick="getRegisters(${data.id})">Modificar</button>&nbsp;
				      			     <button type="button" class="btn btn-danger btn-sm" onclick="deleteBranch(${data.id})">Eliminar</button>
				      			</div>
				      		</td>
				    	</tr>`;
				    			$("#rowsBranch tbody").append(contentHTML);

			})
		}


	})
}

var contador=0;
function addComicsBranch(){
	$("#rowsBranch tbody").empty();
	$('#modaladdcomics').modal({backdrop: 'static', keyboard: false});
	$("#modaladdcomics").modal("show");
	getBranchEn();
	fetch(urlApi).then(data=>data.json())
		.then((json)=>{
			json.data.results.map(data=>{
				contentHTML=`<tr>
				 				<td>
				 					<input type="text" name="idcomic[]" id="idcomic[]" value="${data.id}-${contador}">
				 				</td>
				      			<td>      
				      				<div class="checkbox">
  										<input type="checkbox" name="idchecklist[]"  id="idchecklist[]" onclick="ckeckcomic();" value="1-${contador}">
									</div>
								</td>
				      			<td>${data.title}</td>
				      			<td>
				      				<div>
										<img src="${data.thumbnail.path}.${data.thumbnail.extension}" class="card-img-top" alt="${data.title}" style="margin-top:12px;height: 154px;width: 40%;margin-left: 24%; !important">
				      				</div>
				      			</td>
				    		</tr>`;
				$("#rowsBranch tbody").append(contentHTML);
				contador++;
			})
	})
}

// AQUI SE ASIGNAN LAS SUCURSALES DENTRO DE UN SELECT
function getBranchEn(){
	$.get(urlgetBranch,(data)=>{
		$("#selectBranch").append("<option selected disabled hidden>Selecciona una una sucursal</option>");
		data.map((data)=>{
			contentHTML=`<option value="${data.id}">${data.code} - ${data.name} </option>`;
			$("#selectBranch").append(contentHTML);
		})
	})
}

// CAMBIO DE FUNCION CUANDO SE SELECCIONA EL CHECK LIST
function ckeckcomic(){
	var checklisgt=$("#idchecklist").val();
	if (checklisgt==1){
		
		$("#idchecklist").val(2);
	}else{
		$("#idchecklist").val(1);
	}
}


// ADD COMICS CON SUCURSAL
$("#addcomics").on('click',(e)=>{
	e.preventDefault();
	$("#alertComic").empty();
	$("#alertSuccess").empty();
	var valnull=$("#selectBranch").val();
	if (valnull==null){
	    contentHTML=`<div class="alert alert-warning  alert-dismissible fade show" role="alert" id="alert">
						<strong>Debes seleccionar una sucursal!</strong> 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>`;
		$("#alertComic").append(contentHTML);
	}else{
		
		var formData = new FormData($("#formcomic")[0]);
	    $.ajax({
	    	headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	    	url:urladdcomic,
	    	type: "post",
	    	dataType: "html",
	    	data: formData,
	    	cache: false,
	    	contentType: false,
	    	processData: false,
	    	success:function(data)
	    	{
	    		
			    switch(data) {
			  		case "1":
			    		$("#alertSuccess").empty();
						contentHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert">
	    	    	 					<strong>Debes seleccionar al menos un comic!</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>`;
									$("#alertSuccess").append(contentHTML);
			    	break;
			  		case "2":
		    			$("#alertSuccess").empty();
	    				contentHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert" id="alert">
								  		<strong>Datos guardados correctamente!</strong> 
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>`;
	    				$("#alertSuccess").append(contentHTML);
	    				$("#modaladdcomics").modal("hide");
	    				location.reload()
			    	break;
				}
	    	},
	   		error:function(data)
	    	{
	    	 	contentHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert">
							 	<strong>LLena todos los campos!</strong> 
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>`;
	    		$("#alertSuccess").append(contentHTML);
	    	}
	  	})
	}
})



typeData();