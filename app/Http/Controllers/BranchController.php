<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sucursal;
use App\Inventario;

class BranchController extends Controller
{
    public function addbranch(Request $request){
    	if (empty($request->id)) {
    
	    	$Sucursal = new Sucursal;
	    	$Sucursal->code=$request->code;
	    	$Sucursal->name=$request->name;
	    	$Sucursal->address=$request->address;
	    	$Sucursal->email=$request->email;
	    	$Sucursal->save();
    	}else{
    		$Sucursal=Sucursal::where('id',$request->id)->first();
    		$Sucursal->code=$request->code;
	    	$Sucursal->name=$request->name;
	    	$Sucursal->address=$request->address;
	    	$Sucursal->email=$request->email;
	    	$Sucursal->save();

    	}
    	return response()->json($Sucursal);
	}
	public function getbranch(){
		$getBranch=Sucursal::select('id','code','name','address','email')->orderBy("id", "DESC")->get();
		return response()->json($getBranch);
	}
	public function getregisterbranch(Request $request){
		$id=$request->id;
		$getRegisterBranch=Sucursal::select('id','code','name','address','email')->where('id',$id)->get();
		return response()->json($getRegisterBranch);
	}
	public function deleteBranch(Request $request){
		$id=$request->id;
		$deleteBranch=Sucursal::where('id',$id)->delete();
		return response()->json($deleteBranch);
	}
	public function getBranchesUnic(Request $request){
		$code=$request->id;
		if (empty($code)) {
			$getRegisterBranch=Sucursal::select('id','code','name','address','email')->orderBy("id", "DESC")->get();
			$val=true;
		}else{
			$getRegisterBranch=Sucursal::select('id','code','name','address','email')->where('code',$code)->get();
			$val=false;
		}
		$data=array("getRegisterBranch"=>$getRegisterBranch,"val"=>$val);
		
		return response()->json($data);
	}
	public function addcomics(Request $request){
		if (empty($request->idchecklist)) {
			$data=1;
		}else{
			foreach ($request->idcomic as $register) {
				$Inventario=new Inventario;
				$Inventario->idsucursal=$request->selectBranch;

				$desc=explode("-", $register);
				$idcomi=$desc[1];
				$iddcomic=$desc[0];

				foreach ($request->idchecklist as $register1) {
					$descckeck=explode("-", $register1);
					$idcheck=$descckeck[1];
					$iddckek=$descckeck[0];
					if ($idcomi==$idcheck) {
						$Inventario->idcomic=$iddcomic;
						$Inventario->check=$iddckek;
						$Inventario->save();
					}
				}
			}
			$data=2;
		}
		return response()->json($data);
	}
	public function getBranchDetail(Request $request){
		$idcomic=$request->idcomic;
		$getDetailBranch=Inventario::where("idcomic",$idcomic)->get();
		$datadetails = array();
		foreach ($getDetailBranch as $registro) {
			$nameBranch=Sucursal::where("id",$registro->idsucursal)->get();
			$datadetails[]=array(
								'name'=>$nameBranch[0]->name,	
								);	
		}
		return response()->json($datadetails);

		
	}
}
