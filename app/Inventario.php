<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
 protected $table = 'inventarios';//necesario para reconocer la tabla
    protected $fillable = [
    	"idsucursal",
    	"idcomic",
    	"check"		
    ];
}
