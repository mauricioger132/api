@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-12">
        <div class="card">
          <div class="card-header" style="background-color: #212529!important">
            <div>
              <button type="button" class="btn btn-primary" onclick="addComicsBranch()" >Asignar comics</button>
            </div>
          </div>
          <div class="card-body" style="background-color:#e00a0a !important ">
            <div class="row"  id="comicsRows"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div class="col-lg-12">
    <div class="modal fade" id="modalcomics" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Información detallada</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="table-responsive">
              <table class="table" id="comicstable" ></table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12" >
    <div class="modal fade" id="modaladdcomics" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="height:650px !important">
          <div class="modal-header"></div>
            <div class="modal-body">
              <form id="formcomic" enctype="multipart/form-data" > 
                <div class=" col-lg-12">
                  <div class="form-group row">
                    <label for="Codigo" class="col-md-4 col-form-label text-md-right">Agrega comics a una sucursal</label>
                    <div class="col-md-6">
                      <select name="selectBranch" id="selectBranch" class="form-control" required=""></select>
                    </div>
                  </div>
                </div>
                <div id="alertComic"></div>
                <div id="alertSuccess"></div>
                <div class="table-responsive table-wrapper-scroll-y my-custom-scrollbar " style="height: 350px !important">
                  <table class="table table-bordered rowsBranch"  id="rowsBranch">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">id</th>
                        <th scope="col">Agregar</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Imagen</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
              </form>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick ="clearAlert()">Cerrar</button>
            <button type="button" class="btn btn-secondary" id="addcomics">Guardar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
