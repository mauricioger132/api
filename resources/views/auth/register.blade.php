@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header" style="background-color: #2b2929 !important">
          <div class="col-lg-12">
            <div class="col-lg-12">
              <div class="alert alert-primary" role="alert">
                Ingresa los datos para registrar una nueva sucursal !
              </div>
              <div >
                <button type="button" class="btn btn-primary" onclick="modalbranch()" >Modificar registros</button>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div id="alert"></div>
            <form id="registerBranch">
              <div class="form-group row" hidden>
                <label for="id" class="col-md-4 col-form-label text-md-right">Id</label>
                  <div class="col-md-6">
                    <input  type="hidden" class="form-control" name="id" id="id">
                  </div>
              </div>
              <div class="form-group row">
                <label for="Codigo" class="col-md-4 col-form-label text-md-right">Código de sucursal</label>
                <div class="col-md-6">
                  <input id="code" type="text" class="form-control" name="code" required autocomplete="name" autofocus>
                </div>
              </div>
              <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Nombre de la sucursal</label>
                <div class="col-md-6">
                  <input id="name" type="text" class="form-control" name="name" required autocomplete="name" autofocus>
                </div>
              </div>
              <div class="form-group row">
                <label for="address" class="col-md-4 col-form-label text-md-right">Dirección</label>
                <div class="col-md-6">
                  <input id="address" type="text" class="form-control" name="address" required autocomplete="name" autofocus>
                </div>  
              </div>
              <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">Correo electrónico</label>
                <div class="col-md-6">
                  <input id="email" type="text" class="form-control  @error('email') is-invalid @enderror" name="email" required autocomplete="name" autofocus>
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary" id="addBranch">
                          Registrar
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="modal fade" id="modalbranchs" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
              <div id="alertBranch"></div>
              <div class=" col-lg-8">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" name="getRegisterUnic" id="getRegisterUnic" placeholder="Escribe el código de sucursal" aria-label="Recipient's username" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" onclick="getRegistersEn()">Buscar</button>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered rowsBranch" id="rowsBranch">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">Código</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Dirección</th>
                      <th scope="col">Correo</th>
                      <th scope="col">Opciones</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick ="clearAlert()">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
