<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('addbranch', 'BranchController@addbranch')->name('addbranch');
Route::get('getbranch', 'BranchController@getbranch')->name('getbranch');
Route::get('getregisterbranch', 'BranchController@getregisterbranch')->name('getregisterbranch');
Route::get('deleteBranch', 'BranchController@deleteBranch')->name('deleteBranch');
Route::get('getBranchesUnic', 'BranchController@getBranchesUnic')->name('getBranchesUnic');
Route::post('addcomics', 'BranchController@addcomics')->name('addcomics');
Route::post('addcomics', 'BranchController@addcomics')->name('addcomics');
Route::get('getBranchDetail', 'BranchController@getBranchDetail')->name('getBranchDetail');
